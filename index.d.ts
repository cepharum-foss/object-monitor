
declare module 'object-monitor' {
    interface OriginalData<T> extends Object {}

    /**
     * Controls cloning of monitor instance.
     */
    interface CloningOptions<T> {
        /**
         * Controls if resulting monitor is working recursively or not.
         *
         * @default false
         */
        recursive?: boolean;

        /**
         * Extracts data observed by current monitor as provided for being
         * observed by another monitor.
         *
         * @param monitor monitor instance observing data to extract from
         * @returns data to be observed by cloned monitor instance
         */
        cloneFn?( monitor: Monitor<T> ): T;
    }

    interface MonitoringContext<T> {
        /**
         * Maps recently changed properties of original object into either one's
         * original value.
         */
        changed: Map<string, any>;

        /**
         * Indicates if object has changed while monitoring it.
         */
        hasChanged: boolean;

        /**
         * Reverts all tracked changes to monitored object.
         *
         * @returns current monitor
         */
        rollBack(): Monitor<T>;

        /**
         * Drops all tracks on recent changes to monitored object.
         *
         * @returns current monitor
         */
        commit(): Monitor<T>;

        /**
         * Adjusts controls for logging/throwing in case same property gets
         * adjusted twice without committing intermittently.
         *
         * @param relax true if logging/throwing should be disabled [default], false otherwise
         * @returns current monitor
         */
        relax( relax: boolean ): Monitor<T>;

        /**
         * Creates another monitor instance observing current one's data or
         * whatever is extracted by a custom callback provided in cloning
         * options.
         *
         * @param options cloning options
         * @returns created monitor
         */
        clone( options?: CloningOptions<T> ): Monitor<T>;
    }

    /**
     * Monitors original object.
     */
    interface Monitor<T> {
        /**
         * Provides monitoring context for inspecting changes to monitored
         * object.
         */
        $context: MonitoringContext<T>;
    }

    /**
     * Describes options for customizing behavior of single monitor instance.
     */
    interface MonitoringOptions {
        /**
         * Controls if monitoring includes nested properties of original object.
         *
         * @default false
         */
        recursive?: boolean;

        /**
         * Selects separator to use in hierarchical path names of monitored
         * properties.
         *
         * @default "."
         */
        separator?: string;

        /**
         * Detects if current value of a property and the value to be assigned
         * are equivalent or not.
         *
         * @param existing current value of property
         * @param toBeAssigned provided and coerced value of property to assign
         * @param pathname hierarchical name property to be updated
         * @returns true if both values are considered equivalent
         * @default none
         */
        customCompare?(existing: any, toBeAssigned: any, pathname: string): boolean;

        /**
         * Controls if a warning is logged whenever a property is adjusted twice
         * without committing changes intermittently.
         *
         * @default true
         */
        warn?: boolean;

        /**
         * Controls if an exception is thrown whenever a property is adjusted
         * twice without committing changes intermittently.
         *
         * @default true
         */
        fail?: boolean;

        /**
         * Maps hierarchical path names of properties into functions for
         * coercing arbitrary values per named property. Property names may
         * start or end with an asterisk to match names by prefix or suffix. A
         * sole asterisk is accepted for declaring a default coercion handler.
         *
         * @default none
         */
        coercion?: { [pathname: string]: (value: any) => any };

        /**
         * Controls if monitoring is limited to own properties of original
         * object in opposition to properties of its prototype chain.
         *
         * @default true
         */
        justOwned?: boolean;
    }

    /**
     * Creates monitor observing provided original object tracking it for
     * changes through regular assignment.
     *
     * @param original original data to observe
     * @param options customizations for adjusting monitoring behavior
     * @returns created monitor
     */
    export default function<T>( original: OriginalData<T>, options?: MonitoringOptions ): Monitor<T>;
}
